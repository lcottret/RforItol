# RforItol

R functions to create [Itol](http://itol.embl.de/) dataset files from tabulated files

Dependencies:RColorBrewer

Uses the RColorBrewer nice color palettes. For choosing a palette name (parameter "set"), type display.brewer.all() in R.

* Input file: a tabulated file with a header and in the first column the node ids in an Itol tree
* Output file: a dataset Itol file 

Three functions from now:
* createRangeMetadata: creates Itol file for displaying [dataset with colored ranges](http://itol.embl.de/help.cgi#ranges)
* createStripMetadata: creates Itol file for displaying [dataset with colored strips](http://itol.embl.de/help.cgi#strip)
* createBinaryDataset: creates Itol file for displaying [binary dataset](http://itol.embl.de/help.cgi#binary)
