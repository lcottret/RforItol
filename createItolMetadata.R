# creates the itol file for coloring labels
createRangeMetadata <-
  function(fileIn,
           sep = "\t",
           colName,
           fileOut = "colors_itol.txt",
           set = "Set3") {
    tab <- read.csv(fileIn,
                    sep = sep,
                    header = TRUE,
                    row.names = 1)
    
    if (!colName %in% colnames(tab)) {
      stop(paste("The column", colName, "does not exit"))
    }
    
    lCols <- getColorsFromColumn(tab, sep, colName, set)
    
    write("TREE_COLORS", fileOut, append = FALSE)
    write("SEPARATOR TAB", fileOut, append = TRUE)
    write("DATA", fileOut, append = TRUE)
    
    dfColors <-
      data.frame(rep("range", nrow(tab)), lCols$colorAll, tab[, colName])
    
    rownames(dfColors) <- rownames(tab)
    
    write.table(
      dfColors,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
  }


# creates the itol file for adding colored strips above the leaves
# fileIn : tabulated file with header and with first columns containint node ids
createStripMetadata <-
  function(fileIn,
           sep = "\t",
           colName,
           fileOut = "colors_itol.txt",
           set = "Set3",
           datasetColor = "#ff0000",
           stripWidth = 50,
           margin = 1) {
    tab <- read.csv(fileIn,
                    sep = sep,
                    header = TRUE,
                    row.names = 1)
    
    if (!colName %in% colnames(tab)) {
      stop(paste("The column", colName, "does not exit"))
    }
    
    lCols <- getColorsFromColumn(tab, sep, colName, set)
    
    write("DATASET_COLORSTRIP", fileOut, append = FALSE)
    write("SEPARATOR TAB", fileOut, append = TRUE)
    write(paste("DATASET_LABEL", colName, sep = "\t"), fileOut, append = TRUE)
    write(paste("COLOR", datasetColor, sep = "\t"), fileOut, append = TRUE)
    write("COLOR_BRANCHES\t0", fileOut, append = TRUE)
    write(paste("LEGEND_TITLE", colName, sep = "\t"), fileOut, append = TRUE)
    
    legendShapes <- t(data.frame(rep(1, length(lCols$colorCats))))
    rownames(legendShapes) <- c("LEGEND_SHAPES")
    
    write.table(
      legendShapes,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
    legendColors <- t(data.frame(lCols$colorCats))
    rownames(legendColors) <- c("LEGEND_COLORS")
    
    write.table(
      legendColors,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
    legendLabels <- t(data.frame(mixedsort(names(lCols$colorCats))))
    rownames(legendLabels) <- c("LEGEND_LABELS")
    
    write.table(
      legendLabels,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
    write(paste("STRIP_WIDTH", stripWidth, sep = "\t"), fileOut, append = TRUE)
    
    write("DATA", fileOut, append = TRUE)
    
    dfColors <-
      data.frame(lCols$colorAll, tab[, colName])
    
    rownames(dfColors) <- rownames(tab)
    
    write.table(
      dfColors,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
  }


####
# creates an itol binary dataset from a file containing boolean values
# Format example:
# ID        Nod Am Ecm
# Mt_LYR3     1  1   0
# Lj_LYS12    1  1   0
# Ca_LYR3     1  1   0
# Ps_LYR3     1  1   0
# Gm_LYR3_1   1  1   0
# Gm_LYR3_2   1  1   0
#
###
createBinaryDataset <-
  function(fileIn,
           sep = "\t",
           fileOut = "binary_dataset.txt",
           label = "Binary dataset",
           set = NULL) {
    require(RColorBrewer)
    
    tab <-
      read.csv(
       fileIn,
        sep = "\t",
        header = TRUE,
        row.names = 1
      )
    
    labels <- colnames(tab)
    
    if (!is.null(set)) {
      if (!set %in% rownames(brewer.pal.info))
      {
        stop("This set does not exist in RcolorBrewer")
      }
      
      if (ncol(tab) > brewer.pal.info[set, "maxcolors"])
      {
        stop(paste("Too many categories in the column for the color set selected"))
      }
      
      colors <- brewer.pal(ncol(tab), set)
    }
    else {
      colors <- rep("grey", ncol(tab))
    }
    
    shapes <- rep(1:6, length.out = ncol(tab))
    
    write("DATASET_BINARY", fileOut, append = FALSE)
    write("SEPARATOR TAB", fileOut, append = TRUE)
    write(paste("DATASET_LABEL", label, sep = "\t"), fileOut, append = TRUE)
    write("COLOR\tpurple", fileOut, append = TRUE)
    
    dfShapes = t(data.frame("FIELD_SHAPES" = shapes))
    write.table(
      dfShapes,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
    dfLabels = t(data.frame("FIELD_LABELS" = colnames(tab)))
    write.table(
      dfLabels,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
    dfColors = t(data.frame("FIELD_COLORS" = colors))
    
    write.table(
      dfColors,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
    write("SHOW_LABELS\t1", fileOut, append = TRUE)
    
    write("DATA", fileOut, append = TRUE)
    
    write.table(
      tab,
      file = fileOut,
      append = TRUE,
      quote = FALSE,
      sep = "\t",
      row.names = TRUE,
      col.names = FALSE
    )
    
  }


# creates a vector of colors from column values
getColorsFromColumn <- function(tab, sep = "\t", colName, set) {
  require(RColorBrewer)
  require(gtools)
  
  cats <- mixedsort(levels(tab[, colName]))
  
  
  if (!set %in% rownames(brewer.pal.info))
  {
    stop("This set does not exist in RcolorBrewer")
  }
  
  if (length(cats) > brewer.pal.info[set, "maxcolors"]) {
    stop(paste("Too many categories in the column for the color set selected"))
  }
  
  colorCats <- brewer.pal(length(cats), set)
  
  names(colorCats) <- cats
  
  return(list(
    colorCats = colorCats,
    colorAll =
      sapply(
        X = tab[, colName],
        FUN = function(i) {
          colorCats[which(names(colorCats) == i)]
        },
        simplify = TRUE
      )
  ))
  
  
}